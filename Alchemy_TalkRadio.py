__author__ = 'galenstocking'

from sqlalchemy import Column, ForeignKey, create_engine, Table, MetaData
from sqlalchemy.dialects.mysql import BIGINT, TEXT, SMALLINT, VARCHAR, INTEGER, DATE
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, backref

engine = create_engine('mysql://root@127.0.0.1/news', pool_recycle=3600)
Session = sessionmaker(bind=engine)
base = declarative_base(bind=engine)

class TalkRadioText(base):
    __tablename__ = "TR_texts"
    __table_args__ = {'autoload': True, 'mysql_engine':'InnoDB', 'mysql_charset': 'utf8'}
    id = Column(INTEGER, primary_key=True)
    Title = Column(VARCHAR(300), nullable=False, unique=False)
    Pub_Date = Column(DATE, nullable=False, unique=False)
    Body_Text = Column(TEXT, nullable=False, unique=False)
    Link = Column(VARCHAR(1000), nullable=False, unique=False)
    Show_Id = Column(INTEGER, ForeignKey('TR_Shows.id'))
    Issue_Id = Column(INTEGER, ForeignKey('TR_Issues.id'))

    def __init__(self, title, pub_date, text, link, show_id, issue_id):
        self.Title = title
        self.Pub_Date = pub_date
        self.Body_Text = text
        self.Link = link
        self.Show_Id = show_id
        self.Issue_Id = issue_id

class TalkRadioShow(base):
    __tablename__ = "TR_Shows"
    __table_args__ = {'autoload': True, 'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}
    id = Column(INTEGER, primary_key=True)
    Show_Name = Column(VARCHAR(300), nullable=False)
    TalkRadioTexts = relationship("TalkRadioText", backref="TR_texts")

    def __init__(self, Name):
        self.Outlet_Name = Name

    def SaveIfNotExist(self, session):
        dbInstance = session.query(self.Outlet_Name)
        print "Query results: " + str(dbInstance.count())
        if (dbInstance.count == 0):
            session.add(self)
            session.commit()

class TalkRadioIssue(base):
    __tablename__ = "TR_issues"
    __table_args__ = {'autoload': True, 'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}
    id = Column(INTEGER, primary_key=True)
    Issue_Name = Column(VARCHAR(100), nullable=False)
    SearchTerm = Column(VARCHAR(200), nullable=False)
    TalkRadioTexts = relationship("TalkRadioText", backref="TR_texts")

    def __init__(self, Issue_Name, SearchTerm):
        self.Issue_Name = Issue_Name
        self.SearchTerm = SearchTerm

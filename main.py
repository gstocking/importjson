__author__ = 'galenstocking'

import simplejson
import argparse
from pprint import pprint
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker

strPath = '/Users/galenstocking/Dropbox/grad school/Dissertation/Data/Talk Radio/'

def SafeSaveShow(show):
    ev = session.query(Alchemy_TalkRadio.TalkRadioShow).filter(Alchemy_TalkRadio.TalkRadioShow.Show_Name == show.Show_Name).first()
    if not ev:
        #Saving
        session.add(show)
        session.commit
        ev = session.query(Alchemy_TalkRadio.TalkRadioShow).filter(Alchemy_TalkRadio.TalkRadioShow.Show_Name == show.Show_Name).first()
    # Now return results
    return ev.id


def SafeSaveIssue(issue):
    ev = session.query(Alchemy_TalkRadio.TalkRadioIssue).filter(Alchemy_TalkRadio.TalkRadioIssue.Issue_Name == issue.Issue_Name).first()
    if not ev:
        #Saving
        print "Adding the issue to the database."
        session.add(issue)
        session.commit
        ev = session.query(Alchemy_TalkRadio.TalkRadioIssue).filter(Alchemy_TalkRadio.TalkRadioIssue.Issue_Name == issue.Issue_Name).first()
    # Now return results
    return ev.id

def SaveTranscript(post, issue_id, show_id):
    Transcript = (Alchemy_TalkRadio.TalkRadioText(post['title'], post['postDateTime'],
                                                  post['text'], post['link'], show_id, issue_id)
                 )
    session.add(Transcript)
    session.commit()

def ProcessFile(fn, issue, show):
    Show = Alchemy_TalkRadio.TalkRadioShow(show)
    show_id = SafeSaveShow(Show)

    Issue = Alchemy_TalkRadio.TalkRadioIssue(issue)
    issue_id = SafeSaveIssue(Issue)

    FNFull = strPath + fn
    with open(FNFull) as json_file:
        result = simplejson.load(json_file)
    for post in result:
        SaveTranscript(post, issue_id, show_id)
        #pprint(result[1]['link'])
        #pprint(post['link'])



if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="JSONImport")
    #parser.add_argument("Mode", help="Mode: Either (L)imbaugh or (H)annity")
    parser.add_argument("filename", help="Name of file to process.")
    parser.add_argument("Issue", help="Name of issue.  Can use \"\"")
    #parser.add_argument("SearchTerm", help="Search Term used.")
    args = parser.parse_args()
    engine = create_engine('mysql://root@127.0.0.1/news', pool_recycle=3600)
    Session = sessionmaker(bind=engine)
    session = Session()
    
    strShow="Limbaugh"
    ProcessFile(args.filename, args.Issue, strShow)